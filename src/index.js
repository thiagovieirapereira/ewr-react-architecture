import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Navbar from './components/layout/Navbar/Navbar'
import Alert from './components/layout/Alert/Alert'
import Company from './components/companies/Company/Company'
import Login from './components//auth/Login'
import Home from './pages/Home/Home'
import GlobalStyle from './styles/global'
import Container from './styles/Container'

ReactDOM.render(
  <>
    <GlobalStyle />
    <Router>
      <Navbar />
      <Container>
        <Switch>
          <Route exact path='/home' component={Home} />
          <Route exact path='/' component={Login} />
          <Route exact path='/company/:id' component={Company} />
        </Switch>
        <Alert />
      </Container>
    </Router>
  </>,
  document.getElementById('root')
);
