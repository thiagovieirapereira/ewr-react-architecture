import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom';
import AuthContext from '../../store/auth/authContext'

const PrivateRoute = ({ component: Component, ...rest }) => {
  const authContext = useContext(AuthContext);

  const { isAuthenticated } = authContext

  return (
    <Route {...rest} render={props => isAuthenticated === false ? (
      <Redirect to='/login' />
    ) : (
        <Component {...props} />
      )} />
  )
}

PrivateRoute.propTypes = {
  component: PropTypes.object.isRequired
}

export default PrivateRoute
