import React, { useState } from 'react'
import logo from '../../assets/images/logoHome.png';
import emailIcon from '../../assets/images/mail-outlined.svg'
import lockIcon from '../../assets/images/bx-lock-open-alt.svg'

import * as S from './styles'

const Login = (props) => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();

    if (email === '' || password === '') {
      alert('Preencha todos os campos');
      return
    }

    props.history.push('/home')
  }
  return (
    <S.FormContainer>
      <S.FormLogo src={logo} alt='logo iOasys' className='logo' />
      <S.FormHeading />
      <S.FormParagraph>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </S.FormParagraph>
      <S.FormLogin onSubmit={onSubmit}>
        <S.FormGroup>
          <S.FromLabel htmlFor='email'>Email</S.FromLabel>
          <S.FormIcon src={emailIcon} alt='ícone email' />
          <S.FormInput type='email' name='email' value={email} onChange={(e) => { setEmail(e.target.value) }} />
        </S.FormGroup>
        <S.FormGroup>
          <S.FromLabel htmlFor='password'>Senha</S.FromLabel>
          <S.FormIcon src={lockIcon} alt='ícone cadeado' />
          <S.FormInput type='password' name='password' value={password} onChange={(e) => { setPassword(e.target.value) }} />
        </S.FormGroup>
        <S.FormButton type='submit'>ENTRAR</S.FormButton>
      </S.FormLogin>
    </S.FormContainer>
  )
}

export default Login