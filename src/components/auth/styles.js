import styled from 'styled-components'

export const FormContainer = styled.div`
    display:flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    max-width: 500px;
    margin: 2rem auto;
    overflow: hidden;
    padding: 0 2rem;
`

export const FormLogo = styled.img`
        width: 80%;
        margin-bottom: 1.5rem
`

export const FormHeading = styled.h1`
  font-size: 2rem;
  margin-bottom: 1.5rem;
`

export const FormParagraph = styled.p`
        font-size: 1.25rem;
`

export const FormGroup = styled.div`
              margin: 1.2rem 0;
            position: relative;
`

export const FormLogin = styled.form`
        width: 100%;
`

export const FormInput = styled.input`
            display: block;
            width: 100%;
            padding: 0.4rem;
            padding-left: 2.5rem;
            font-size: 1.2rem;
            background-color: transparent;
            border: none;
            border-bottom: 2px solid #333;
            transition: all ease 0.3s;
            margin: 0.2rem 0;


            &:focus {
                border-bottom: 2px solid #ee4c77;
                outline: none;
            }
`

export const FromLabel = styled.label`
            display: none;
`

export const FormIcon = styled.img`
            width: 1rem;
            position: relative;
            top: 2.25em;
            left: 0.5rem;
`

export const FormButton = styled.button`
              display: block;
            width: 100%;
            padding: 0.5rem 0;
            border-radius: 1.8px;
            border: none;
            background-color: #57bbbc;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            font:inherit;
            box-sizing: border-box;
            margin: 0;

          &:focus {
              outline: none;
          }
`