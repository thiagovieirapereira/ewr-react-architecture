import React from 'react'
import CompanyItem from '../CompanyItem/CompanyItem'
import companiesList from '../../../mock/companies.json'

import * as S from './styles'

const Companies = () => {

  const { enterprises } = companiesList

  return (
    <S.CompaniesGrid>
      {enterprises.map(enterprise => (
        < CompanyItem key={enterprise.id} companieProp={enterprise} />
      ))}

    </S.CompaniesGrid>
  )
}

export default Companies
