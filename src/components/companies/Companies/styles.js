import styled from 'styled-components'

export const CompaniesGrid = styled.div`
    
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;

    @media (max-width: 700px) {
        grid-template-columns: 1fr;
    }
`