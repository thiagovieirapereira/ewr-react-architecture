import React, { useState } from 'react'
import companiesTypes from './companiesTypes'
import CompanyItem from '../CompanyItem/CompanyItem'
import companiesList from '../../../mock/companies.json'

import * as C from '../Companies/styles'
import * as S from './styles'

const Search = () => {
  const [text, setText] = useState('');
  const [select, setSelect] = useState('Agro');
  const [companies, setCompanies] = useState([]);

  const searchCompanies = () => {
    const { enterprises } = companiesList

    const categoryFilter = enterprises.filter(category => category.enterprise_type.enterprise_type_name.includes(`${select}`))

    const nameFilter = categoryFilter.filter(name => name.enterprise_name.toLowerCase().includes(text.toLowerCase()))

    setCompanies(nameFilter);
  }

  const onSubmit = (e) => {
    e.preventDefault();

    if (text === '') {
      alert('Por favor insira o nome de uma empresa');
      return
    }
    searchCompanies()
  }

  const onChange = (e) => setText(e.target.value)
  const onSelect = (e) => setSelect(e.target.value)

  return (
    <>
      <S.Form onSubmit={onSubmit}>
        <label>
          Escolha o setor da empresa
          <select onChange={onSelect} placeholder="Selecione um tipo de empresa...">
            {Object.entries(companiesTypes).map(([key, value]) => <option key={key} value={value}>{value}</option>)}
          </select>
        </label>
        <input type='text' name='text' placeholder='Escreva o nome de uma empresa...' value={text} onChange={onChange} />
        <input type='submit' value='Pesquisar' className='btn btn-dark btn-block' />
      </S.Form>
      <C.CompaniesGrid>
        {companies.length === 0 ? (<span>Sem empresas para mostrar...</span>) : (
          companies.map(company => (
            < CompanyItem key={company.id} companieProp={company} />
          ))
        )}

      </C.CompaniesGrid>
    </>
  )
}

export default Search
