import styled from 'styled-components'

export const Form = styled.form`
    input[type='text'],
    select {
        display: inline-block;
        width: 100%;
        padding: 0.4rem;
        font-size: 1.2rem;
        border: 1px solid #ccc;
        margin: 1.2rem 0;
    }

    .btn {
        display: inline-block;
        background: #f4f4f4;
        color: #333;
        padding: 0.4rem 1.3rem;
        margin: 1.2rem 0;
        font-size: 1rem;
        border: none;
        cursor: pointer;
        margin-right: 0.5rem;
        transition: opacity 0.2s ease-in;
        outline: none;

        &-block {
            display: block;
            width: 100%;
        }
    
        &-dark {
            background: #333333;
            color: #fff;
        }
    }

`