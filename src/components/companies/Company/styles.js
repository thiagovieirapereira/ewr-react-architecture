import styled from 'styled-components'

export const CompanyCard = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;
    padding: 1rem;
    border: #ccc 1px dotted;
    margin: 0.7rem 0;
    background-color: #ffffff;

    @media (max-width: 700px)    {
        grid-template-columns: 1fr;
    }

    img {
        width: 100%;       
    }

    p {
        font-size:1rem;
        font-family: Roboto;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: auto;
        text-align: left;
        color: #333;
    }

    h1 {
        font-size:2rem;
        font-family: Roboto;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: auto;
        text-align: left;
        color: #333;
    }
`;

export const BtnBack = styled.button`
    display: inline-block;
    background: #f4f4f4;
    color: #333;
    padding: 0.4rem 1.3rem;
    margin: 1.2rem 0;
    font-size: 1rem;
    border: none;
    cursor: pointer;
    margin-right: 0.5rem;
    transition: opacity 0.2s ease-in;
    outline: none;
`