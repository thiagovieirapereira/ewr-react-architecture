import React from 'react'
import { Link } from 'react-router-dom'
import companyList from '../../../mock/companies.json'

import * as S from './styles'

const Company = ({ match }) => {

  const companyId = parseInt(match.params.id) - 1;

  const companiesData = companyList.enterprises[companyId]

  const { enterprise_name, description } = companiesData;

  return (
    <>
      <Link to='/home' >
        <S.BtnBack>Voltar para busca</S.BtnBack>
      </Link>
      <S.CompanyCard>
        <div>
          <img src='https://source.unsplash.com/collection/1608456/640x480' alt={enterprise_name} style={{ width: '100%' }} />
        </div>
        <div>
          <h1>{enterprise_name}</h1>
          <p>{description}</p>
        </div>
      </S.CompanyCard>
    </>
  )
}

export default Company
