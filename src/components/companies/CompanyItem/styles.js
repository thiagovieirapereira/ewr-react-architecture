import styled from 'styled-components'

export const CompanyCard = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;

    @media (max-width: 700px) {
        grid-template-columns: 1fr;
    }

    padding: 1rem;
    border: #ccc 1px dotted;
    margin: 0.7rem 0;
    background-color: #fff;

    h3, h4, h5 {
        text-align: left;
        font-family: 'Roboto', sans-serif;
    }

    img {
        width: 100%;
    }

    a {
        text-decoration: none
    }

    .btn {
        display: inline-block;
        background: #f4f4f4;
        color: #333;
        padding: 0.4rem 1.3rem;
        margin: 1.2rem 0;
        font-size: 1rem;
        border: none;
        cursor: pointer;
        margin-right: 0.5rem;
        transition: opacity 0.2s ease-in;
        outline: none;

        &-sm {
            font-size: 0.8rem;
            padding: 0.3rem 1rem;
            margin-right: 0.2rem;
        }
    }

    .my-1 {
        margin: 1rem 0;
    }
`