import React from 'react'
import { Link } from 'react-router-dom'

import * as S from './styles'

const CompanyItem = ({ companieProp: { id, enterprise_name, country, enterprise_type } }) => {

  const { enterprise_type_name } = enterprise_type;

  return (
    <S.CompanyCard>
      <img src='https://source.unsplash.com/collection/1608456/640x480' alt={enterprise_name} />
      <div>
        <h3>{enterprise_name}</h3>
        <h4>{enterprise_type_name}</h4>
        <h5>{country}</h5>
        <div>
          <Link to={`/company/${id}`} className='btn btn-sm my-1'>Ver detalhes</Link>
        </div>
      </div>
    </S.CompanyCard>
  )
}

export default CompanyItem
