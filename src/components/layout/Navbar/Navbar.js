import React from 'react'
import logo from '../../../assets/images/logoNav.png';

import * as S from './styles'

const Navbar = () => {
  return (
    <>
      <S.Nav>
        <img src={logo} alt='logo iOasys' />
      </S.Nav>
    </>
  )
}

export default Navbar
