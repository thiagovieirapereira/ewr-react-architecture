import styled from 'styled-components'

export const Nav = styled.nav`
    @import url('https://fonts.googleapis.com/css?family=Roboto');

    display: flex;
    flex-direction: row-reverse;
    justify-content: start;
    align-items: center;
    padding: 0.7rem 2rem;
    z-index: 1;
    width: 100%;
    height: 3rem;
    border-bottom: solid 1px #f4f4f4;
    opacity: 0.9;
    margin-bottom: 1rem;

    background: rgb(238, 76, 119);
    background: linear-gradient(
        180deg,
        rgba(238, 76, 119, 1) 0%,
        rgba(187, 17, 62, 1) 100%
    );

    img {
        max-width: 5rem;
        position: absolute;
        left: 50%;
        transform: translateX(-50%)
    }

    button {
        justify-self: flex-start;

        display: inline-block;
        background: transparent;
        color: #ffffff;
        padding: 0.2rem 1rem;
        font-size: 1rem;
        letter-spacing: normal;
        text-align: center;
        border: none;
        cursor: pointer;
        margin-right: 0.5rem;
        transition: opacity 0.2s ease-in;
        outline: none;
        border-radius: 1.8px;
    }
`