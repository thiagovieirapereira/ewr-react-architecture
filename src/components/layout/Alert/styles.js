import styled from 'styled-components'

export const AlertToast = styled.div`
    padding: 0.7rem;
    margin: 1rem 0;
    opacity: 0.9;
    background: #dc3545;
    color: #fff;
`