import React from 'react'

import * as S from './styles'

const Alert = () => {
  return (
    <S.AlertToast>
      {'alert.msg'}
    </S.AlertToast>
  )
}

export default Alert;