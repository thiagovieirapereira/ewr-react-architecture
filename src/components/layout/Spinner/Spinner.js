import React from 'react'

import * as S from './styles'

const Spinner = () => {
  return (
    <>
      <S.SpinnerLoader />
    </>
  )
}

export default Spinner
