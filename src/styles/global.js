import { createGlobalStyle } from 'styled-components'

const GloabalStyle = createGlobalStyle`
    html{
        font-size: 16px;
    }
    
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
    }

    body {
        margin: 0;
        font-family: 'Roboto', sans-serif;

        background-color: #ebe9d7;
    }
`;

export default GloabalStyle;