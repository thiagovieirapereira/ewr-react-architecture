import styled from 'styled-components'

const Container = styled.div`
    display:flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    
    max-width: 1100px;
    margin: auto;
    overflow: hidden;
    padding: 0 2rem;

    a {
        align-self: flex-start
    }
`

export default Container